import Dispatcher from "./../dispatcher/Dispatcher";
import {AppConstants} from "../constants/AppConstants";
import {AppClient} from "../client/AppClient";

export class AppActions {
    
    static getArticles() {

        return AppClient.getArticles()
            .then((response) => {
                Dispatcher.dispatch({
                    actionType: AppConstants.GET_ARTICLE_SUCCESS,
                    article: response
                });
            }).catch((errorInfo) => {
            Dispatcher.dispatch({
                actionType: AppConstants.GET_ARTICLE_FAIL,
                errorInfo
            });
        });
    }

}