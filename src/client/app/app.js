/* eslint-env browser */
import React from "react";
import {RouterConstants} from "./../app/constants/RouterConstants.js";
import {MainView} from "./views/MainView";
import {AppView} from "./views/AppView";
import {Router, Route, IndexRoute, browserHistory} from "react-router";
export class App extends React.Component {

    constructor(props) {
        super(props);
        this.render = this.render.bind(this);
    }

    render() {
        return (
            <Router history={browserHistory}>
                <Route path="/" component={AppView}>
                    <IndexRoute component={MainView}/>
                </Route>
            </Router>
        );
    }
}