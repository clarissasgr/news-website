import React from "react";
import autobind from "class-autobind";
import {MainComponent} from "./components/MainComponent";
import {AppActions} from "./../actions/AppActions";
import {appStore} from "./../store/appStore";
import news_01 from "../../../images/news_01.jpg";
import news_01_bigger from "../../../images/news_01@2x.png";
import news_02 from "../../../images/news_02.jpg";
import news_02_bigger from "../../../images/news_02@2x.jpg";
import news_03 from "../../../images/news_03.jpg";
import news_03_bigger from "../../../images/news_03@2x.jpg";
import author from "../../../images/author.jpg";
import author_bigger from "../../../images/author@2x.jpg";
import {Col, Grid, Row} from "react-bootstrap";

export class MainView extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            article: appStore.getStateArticle()
        };

        autobind(this);
    }

    componentDidMount() {
        appStore.addChangeListener(this._updateState);

        this._handleGetTechNews();
    }

    componentWillUnmount() {
        appStore.removeChangeListener(this._updateState);
    }

    _updateState() {
        this.setState({
            article: appStore.getStateArticle()
        });
    }

    _handleGetTechNews() {
        setTimeout(() => AppActions.getArticles(), 30000);
    }

    render() {
        return (
            <Grid>
                <Row className="content">
                    <Col xs={12} sm={12} md={12} lg={6}>
                        <MainComponent
                            category="politics"
                            title={this.state.article[0].title}
                            author={this.state.article[0].author}
                            urlToImage={this.state.article[0].urlToImage !== "" ? this.state.article[0].urlToImage : [news_01,news_01_bigger] }
                            authorImage={[author, author_bigger]}
                            authorImageSize="author-image-big"
                            description={this.state.article[0].description}
                            url={this.state.article[0].url}
                        />
                    </Col>
                    <Col xs={12} sm={6} md={6} lg={3}>
                        <MainComponent
                            category="tech"
                            title={this.state.article[1].title}
                            author={this.state.article[1].author}
                            urlToImage={this.state.article[1].urlToImage !== "" ? this.state.article[1].urlToImage : [news_02,news_02_bigger]}
                            authorImage={[author, author_bigger]}
                            authorImageSize="author-image-small"
                            description={this.state.article[1].description}
                            url={this.state.article[1].url}
                        />
                    </Col>
                    <Col xs={12} sm={6} md={6} lg={3}>
                        <MainComponent
                            category="science"
                            title={this.state.article[2].title}
                            author={this.state.article[2].author}
                            urlToImage={this.state.article[2].urlToImage !== "" ? this.state.article[2].urlToImage : [news_03,news_03_bigger]}
                            authorImage={[author, author_bigger]}
                            authorImageSize="author-image-small"
                            description={this.state.article[2].description}
                            url={this.state.article[2].url}
                        />
                    </Col>
                    <div className="clear"></div>
                    <hr/>
                    <Col xs={12} md={12} lg={4}>
                        <MainComponent
                            category="sports"
                            title={this.state.article[3].title}
                            author={this.state.article[3].author}
                            urlToImage=""
                            authorImage={author}
                            authorImageSize="author-image-small"
                            description={this.state.article[3].description}
                            url={this.state.article[3].url}
                        />
                    </Col>
                    <Col xs={12} md={12} lg={4}>
                        <MainComponent
                            category="tech"
                            title={this.state.article[4].title}
                            author={this.state.article[4].author}
                            urlToImage=""
                            authorImage={author}
                            authorImageSize="author-image-small"
                            description={this.state.article[4].description}
                            url={this.state.article[4].url}
                        />
                    </Col>
                    <Col xs={12} md={12} lg={4}>
                        <MainComponent
                            category="science"
                            title={this.state.article[5].title}
                            author={this.state.article[5].author}
                            urlToImage=""
                            authorImage={author}
                            authorImageSize="author-image-small"
                            description={this.state.article[5].description}
                            url={this.state.article[5].url}
                        />
                    </Col>
                </Row>
            </Grid>
        );
    }
}