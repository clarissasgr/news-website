import React from "react";
import RetinaImage from "react-retina-image";

export class MainComponent extends React.Component {

    constructor(props) {
        super(props);
        this.render = this.render.bind(this);
    }

    render() {
        return (
            <div>
                <div className="category">
                    <p className={this.props.category}>{this.props.category}</p>
                </div>
                {this.props.urlToImage !== "" ?
                        <div className="image">
                            <RetinaImage className="top-component-image" src={this.props.urlToImage}/>
                            <a href={this.props.url} target="_blank" className="read-more"><h4>Read more</h4></a>
                        </div>
                    : ""}
                <div>
                    <h1 className={this.props.category}>{this.props.title}</h1>
                </div>
                <div className="author">
                    <RetinaImage className={this.props.authorImageSize} src={this.props.authorImage}/>
                    <span><h5 className="authors-name">by {this.props.author}</h5></span>
                </div>
                <div>
                    <h4 className={this.props.category}>{this.props.description}</h4>
                </div>
            </div>
        );
    }
}