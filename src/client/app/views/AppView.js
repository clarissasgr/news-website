import React from "react";
import {AppBarTop} from "./util/AppBarTop";

/*global window, document*/
export class AppView extends React.Component {

    constructor(props) {
        super(props);
        this.state = {};
        this.render = this.render.bind(this);
    }


    render() {
        return (
            <div>
                <AppBarTop/>
                {this.props.children}
            </div>
        );
    }
}

AppView.propTypes = {
    children: React.PropTypes.element,
    history: React.PropTypes.shape({
        pushState: React.PropTypes.func
    })
};