import React from "react";
import autobind from "class-autobind";
import RetinaImage from "react-retina-image";
import logo from "../../../../images/logo.png";
import logo_bigger from "../../../../images/logo@2x.png";
import menu from "../../../../images/menu.png";
import menu_bigger from "../../../../images/menu@2x.png";

import {Navbar, Nav, NavItem} from "react-bootstrap";

/*global window*/
export class AppBarTop extends React.Component {

    constructor(props) {
        super(props);
        this.state = {};
        autobind(this);
    }

    render() {
        return (
            <Navbar>
                <Navbar.Header>
                    <span id="smallMenu">
                        <RetinaImage src={[menu, menu_bigger]}/>
                    </span>
                    <RetinaImage id="logo" src={[logo, logo_bigger]}/>
                </Navbar.Header>
                <Navbar.Collapse>
                    <Nav pullRight>
                        <NavItem><h3>Politics</h3></NavItem>
                        <NavItem><h3>Business</h3></NavItem>
                        <NavItem><h3>Tech</h3></NavItem>
                        <NavItem><h3>Science</h3></NavItem>
                        <NavItem><h3>Sports</h3></NavItem>
                    </Nav>
                </Navbar.Collapse>
            </Navbar>
        );
    }
}