export let AppConstants = {
    GET_ARTICLE_SUCCESS: "get_article_success",
    GET_ARTICLE_FAIL: "get_article_fail"
};