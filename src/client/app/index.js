import React from "react";
import {App} from "./app";
import ReactDOM from "react-dom";
import styles from "./../../style.css";

function render(Component) {
    const rootElement = document.getElementById("app");
    return new Promise((resolve) => {
        ReactDOM.render((Component), rootElement, resolve());
    });
}

render(<App/>);