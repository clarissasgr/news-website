import Store from "./Store";
import Dispatcher from "../dispatcher/Dispatcher";
import {AppConstants} from "./../constants/AppConstants";

let appStoreState = {
    article: [
        {
            category: "politics",
            title: "Obama Offers Hopeful Vision While Noting Nation's Fears",
            author: "Creed Bratton",
            urlToImage: "",
            authorImage: "",
            authorImageSize: "author-image-big",
            description: "In his last State of the Union address, President Obama sought to paint a hopeful portrait. " +
            "But he acknowledged that many Americans felt shut out of a political and economic system they view as rigged."
        },
        {
            category: "tech",
            title: "Didi Kuaidi, The Company Beating Uber In China, Opens Its API To Third Party Apps",
            author: "Creed Bratton",
            urlToImage: "",
            authorImage: "",
            authorImageSize: "author-image-small",
            description: "One day after Uber updated its API to add ‘content experiences’ for passengers, " +
            "the U.S. company’s biggest rival — Didi Kuaidi in China — has opened its own platform " +
            "up by releasing an SDK for developers and third-parties."
        },
        {
            category: "science",
            title: "NASA Formalizes Efforts To Protect Earth From Asteroids",
            author: "Alexandre Henrique Shailesh Zeta-Jones",
            urlToImage: "",
            authorImage: "",
            authorImageSize: "author-image-small",
            description: "Last week, NASA announced a new program called the Planetary Defense Coordination Office" +
            "(PDCO) which will coordinate NASA’s efforts for detecting and tracking near-Earth objects (NEOs)." +
            "If a large object comes hurtling toward our planet…."
        },
        {
            category: "sports",
            title: "For Some Atlanta Hawks, a Revved-Up Game of Uno Is Diversion No. 1",
            author: "Creed Bratton",
            urlToImage: "",
            authorImage: "",
            authorImageSize: "author-image-small",
            description: "The favored in-flight pastime of a group of players" +
            "including Al Horford, Kent Bazemore and Dennis Schroder is a schoolchildren’s card game with some added twists."
        },
        {
            category: "tech",
            title: "Picking a Windows 10 Security Package",
            author: "Creed Bratton",
            urlToImage: "",
            authorImage: "",
            authorImageSize: "author-image-small",
            description: "Oscar the Grouch has a recycling bin and Big Bird has moved to a tree as the children’s " +
            "classic debuts on HBO, aiming at a generation that doesn’t distinguish between TV and mobile screens."
        },
        {
            category: "science",
            title: "As U.S. Modernizes Nuclear Weapons, ‘Smaller’ Leaves Some Uneasy",
            author: "Alexandre Henrique Shailesh Zeta-Jones",
            urlToImage: "",
            authorImage: "",
            authorImageSize: "author-image-small",
            description: "The Energy Department and the Pentagon have been readying a weapon with a build-it-smaller approach," +
            " setting off a philosophical clash in the world of nuclear arms."
        }
    ]
};

class AppStore extends Store {

    constructor() {
        super();
    }

    getStateArticle() {
        return appStoreState.article;
    }

}

let instance = new AppStore();

Dispatcher.register((action) => {
    let doEmitChange = true;
    switch (action.actionType) {
        case AppConstants.GET_ARTICLE_SUCCESS:
            appStoreState.article = action.article;
            break;

        case AppConstants.GET_ARTICLE_FAIL:
            console.log("Erro ao buscar artigos.");
            break;
        
        default:
            doEmitChange = false;
            break;
    }

    if (doEmitChange) {
        instance.emitChange();
    }

    return true;
});

export let appStore = instance;