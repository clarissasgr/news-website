import fetch from "isomorphic-fetch";
import autobind from "class-autobind";

export class AppClient {

    constructor() {
        autobind(this);
    }

    static getArticles() {
        return new Promise((resolve, reject) => {
            fetch("http://newswebsite.pythonanywhere.com/article/", {
                method: "GET",
                headers: {
                    "Accept": "application/json",
                    "Content-Type": "application/json; charset=utf-8"
                }
            }).then((response) => {
                response.json().then((response) => {
                    resolve(response);
                });
            }).catch((errorInfo) => {
                reject(errorInfo);
            });
        });
    }

}
