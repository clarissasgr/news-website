var path = require('path');
var webpack = require('webpack');

var BUILD_DIR = path.resolve(__dirname, 'src/client/public');
var APP_DIR = path.resolve(__dirname, 'src/client/app');

var loaders = [
    {
        test: /\.(html)$/,
        loader: 'file?name=[name].[ext]'
    },
    {
        test: /\.css/,
        loader: "style-loader!css-loader"
    },
    {
        test: /\.(eot|woff|woff2|ttf|svg|png|jpg)$/,
        loader: 'file-loader?name=assets/images/[name].[ext]'
    },
    {
        test: /\.json$/,
        loader: 'json'
    },
    {
        loader: 'babel-loader',
        test: /\.jsx?$/,
        include: [
            /src/
        ],
        query: {
            babelrc: false,
            cacheDirectory: true,
            presets: ["es2015", "react"],
            plugins: ["transform-object-rest-spread", "transform-react-remove-prop-types", "transform-react-constant-elements"]
        }
    }
];

var config = {
    devtool: 'source-map',
    entry: {
        "app": ["babel-polyfill", APP_DIR + "/index.js"]
    },
    output: {
        path: BUILD_DIR,
        filename: '[name].js'
    },
    resolve: {
        modulesDirectories: ['node_modules']
    },
    node: {
        fs: "empty"
    },
    module: {
        preLoaders: [
            {
                test: /\.jsx?$/,
                loader: 'string-replace',
                include: [
                    /src.app/
                ]

            },
            {
                test: /\.jsx?$/,
                loaders: ['eslint'],
                include: [
                    /src.app/
                ]
            }
        ],
        loaders: loaders
    }
};

module.exports = config;